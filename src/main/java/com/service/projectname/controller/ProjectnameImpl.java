package com.service.projectname.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-01-19T10:58:59.092+08:00")

@RestSchema(schemaId = "projectname")
@RequestMapping(path = "/projectName", produces = MediaType.APPLICATION_JSON)
public class ProjectnameImpl {

    @Autowired
    private ProjectnameDelegate userProjectnameDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectnameDelegate.helloworld(name);
    }

}
